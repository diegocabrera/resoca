var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"cliente","name":"cliente"},{"data":"factura","name":"factura"},{"data":"forma_pago","name":"forma_pago"},{"data":"tipo_pago","name":"tipo_pago"},{"data":"n_transaccion","name":"n_transaccion"},{"data":"monto_pago","name":"monto_pago"},{"data":"recibo","name":"recibo"},{"data":"retencion","name":"retencion"},{"data":"fecha_pedido","name":"fecha_pedido"},{"data":"fecha_pago","name":"fecha_pago"},{"data":"fecha_recibo","name":"fecha_recibo"},{"data":"fecha_envio","name":"fecha_envio"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});