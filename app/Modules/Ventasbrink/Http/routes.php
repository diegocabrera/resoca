<?php

Route::group(['middleware' => 'web', 'prefix' => 'ventasbrink', 'namespace' => 'App\\Modules\Ventasbrink\Http\Controllers'], function()
{
    //Route::get('/', 'VentasbrinkController@index');

    Route::get('/', 				'VentasbrinkController@index');
    Route::get('buscar/{id}', 		'VentasbrinkController@buscar');
    Route::post('guardar', 			'VentasbrinkController@guardar');
    Route::put('guardar/{id}', 		'VentasbrinkController@guardar');
    Route::delete('eliminar/{id}', 	'VentasbrinkController@eliminar');
    Route::post('restaurar/{id}', 	'VentasbrinkController@restaurar');
    Route::delete('destruir/{id}', 	'VentasbrinkController@destruir');
    Route::get('arbol', 			'VentasbrinkController@arbol');
    Route::get('datatable', 		'VentasbrinkController@datatable');
});
