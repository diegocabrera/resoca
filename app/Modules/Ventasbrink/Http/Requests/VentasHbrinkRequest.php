<?php

namespace App\Modules\Ventasbrink\Http\Requests;

use App\Http\Requests\Request;

class VentasHbrinkRequest extends Request {
    protected $reglasArr = [
		'cliente' => ['required', 'min:3', 'max:255'], 
		'factura' => ['integer'], 
		'forma_pago' => ['required', 'min:3', 'max:255'], 
		'tipo_pago' => ['required', 'min:3', 'max:255'], 
		'n_transaccion' => ['integer'], 
		'monto_pago' => ['required'], 
		'recibo' => ['integer'], 
		'fecha_pedido' => ['required']
	];
}