<?php

namespace App\Modules\Ventasbrink\Http\Controllers;


//Controlador Padre
use App\Modules\Ventasbrink\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Ventasbrink\Http\Requests\VentasHbrinkRequest;

//Modelos
use App\Modules\Ventasbrink\Models\VentasHbrink;

class VentasbrinkController extends Controller
{
    protected $titulo = 'Herramientas Brink';

    public $js = [
        'VentasHbrink'
    ];

    public $css = [
        'VentasHbrink'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('ventasbrink::VentasHbrink', [
            'VentasHbrink' => new VentasHbrink()
        ]);
    }

    public function nuevo()
    {
        $VentasHbrink = new VentasHbrink();
        return $this->view('ventasbrink::VentasHbrink', [
            'layouts' => 'base::layouts.popup',
            'VentasHbrink' => $VentasHbrink
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $VentasHbrink = VentasHbrink::find($id);
        return $this->view('ventasbrink::VentasHbrink', [
            'layouts' => 'base::layouts.popup',
            'VentasHbrink' => $VentasHbrink
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $VentasHbrink = VentasHbrink::withTrashed()->find($id);
        } else {
            $VentasHbrink = VentasHbrink::find($id);
        }

        if ($VentasHbrink) {
            return array_merge($VentasHbrink->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(VentasHbrinkRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            dd('entro al metodo');
            $VentasHbrink = $id == 0 ? new VentasHbrink() : VentasHbrink::find($id);

            $VentasHbrink->fill($request->all());
            $VentasHbrink->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $VentasHbrink->id,
            'texto' => $VentasHbrink->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            VentasHbrink::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            VentasHbrink::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            VentasHbrink::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = VentasHbrink::select([
            'id', 'cliente', 'factura', 'forma_pago', 'tipo_pago', 'n_transaccion', 'monto_pago', 'recibo', 'retencion', 'fecha_pedido', 'fecha_pago', 'fecha_recibo', 'fecha_envio', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}
