<?php

namespace App\Modules\Ventasbrink\Models;

use App\Modules\base\Models\Modelo;



class VentasHbrink extends modelo
{
    protected $table = 'ventas_hbrink';
    protected $fillable = ["cliente","factura","forma_pago","tipo_pago","n_transaccion","monto_pago","recibo","retencion","fecha_pedido","fecha_pago","fecha_recibo","fecha_envio"];
    protected $campos = [
    'cliente' => [
        'type' => 'text',
        'label' => 'Cliente',
        'placeholder' => 'Cliente del Ventas Hbrink'
    ],
    'factura' => [
        'type' => 'number',
        'label' => 'Factura',
        'placeholder' => 'Factura del Ventas Hbrink'
    ],
    'forma_pago' => [
        'type' => 'text',
        'label' => 'Forma Pago',
        'placeholder' => 'Forma Pago del Ventas Hbrink'
    ],
    'tipo_pago' => [
        'type' => 'text',
        'label' => 'Tipo Pago',
        'placeholder' => 'Tipo Pago del Ventas Hbrink'
    ],
    'n_transaccion' => [
        'type' => 'number',
        'label' => 'N Transaccion',
        'placeholder' => 'N Transaccion del Ventas Hbrink'
    ],
    'monto_pago' => [
        'type' => 'text',
        'label' => 'Monto Pago',
        'placeholder' => 'Monto Pago del Ventas Hbrink'
    ],
    'recibo' => [
        'type' => 'number',
        'label' => 'Recibo',
        'placeholder' => 'Recibo del Ventas Hbrink'
    ],
    'retencion' => [
        'type' => 'text',
        'label' => 'Retencion',
        'placeholder' => 'Retencion del Ventas Hbrink'
    ],
    'fecha_pedido' => [
        'type' => 'text',
        'label' => 'Fecha Pedido',
        'placeholder' => 'Fecha Pedido del Ventas Hbrink'
    ],
    'fecha_pago' => [
        'type' => 'text',
        'label' => 'Fecha Pago',
        'placeholder' => 'Fecha Pago del Ventas Hbrink'
    ],
    'fecha_recibo' => [
        'type' => 'text',
        'label' => 'Fecha Recibo',
        'placeholder' => 'Fecha Recibo del Ventas Hbrink'
    ],
    'fecha_envio' => [
        'type' => 'text',
        'label' => 'Fecha Envio',
        'placeholder' => 'Fecha Envio del Ventas Hbrink'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}