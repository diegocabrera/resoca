@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Ventas Hbrink']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar VentasHbrink.',
        'columnas' => [
            'Cliente' => '8.3333333333333',
		'Factura' => '8.3333333333333',
		'Forma Pago' => '8.3333333333333',
		'Tipo Pago' => '8.3333333333333',
		'N Transaccion' => '8.3333333333333',
		'Monto Pago' => '8.3333333333333',
		'Recibo' => '8.3333333333333',
		'Retencion' => '8.3333333333333',
		'Fecha Pedido' => '8.3333333333333',
		'Fecha Pago' => '8.3333333333333',
		'Fecha Recibo' => '8.3333333333333',
		'Fecha Envio' => '8.3333333333333'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $VentasHbrink->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection