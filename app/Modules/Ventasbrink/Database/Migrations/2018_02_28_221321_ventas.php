<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ventas extends Migration
{
    public function up()
    {
        Schema::create('ventas_hbrink', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cliente');
            $table->integer('factura')->nullable();
            $table->string('forma_pago');
            $table->string('tipo_pago');
            $table->integer('n_transaccion')->nullable();
            $table->decimal('monto_pago', 11,2);
            $table->integer('recibo')->nullable();
            $table->decimal('retencion', 11, 2)->nullable();
            $table->timestamp('fecha_pedido');
            $table->timestamp('fecha_pago')->nullable();
            $table->timestamp('fecha_recibo')->nullable();
            $table->timestamp('fecha_envio')->nullable();


            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas_hbrink');
    }
}
