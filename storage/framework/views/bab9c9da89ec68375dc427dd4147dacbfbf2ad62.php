<?php $__env->startSection('content-top'); ?>
    <?php echo $__env->make('base::partials.botonera', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Ventas Hbrink']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
    <?php echo $__env->make('base::partials.modal-busqueda', [
        'titulo' => 'Buscar VentasHbrink.',
        'columnas' => [
            'Cliente' => '8.3333333333333',
		'Factura' => '8.3333333333333',
		'Forma Pago' => '8.3333333333333',
		'Tipo Pago' => '8.3333333333333',
		'N Transaccion' => '8.3333333333333',
		'Monto Pago' => '8.3333333333333',
		'Recibo' => '8.3333333333333',
		'Retencion' => '8.3333333333333',
		'Fecha Pedido' => '8.3333333333333',
		'Fecha Pago' => '8.3333333333333',
		'Fecha Recibo' => '8.3333333333333',
		'Fecha Envio' => '8.3333333333333'
        ]
    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <?php echo Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]); ?>

            <?php echo $VentasHbrink->generate(); ?>

        <?php echo Form::close(); ?>

    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>