(function($) {
    "use strict";

    $(document).ready(function() {

        /* -------------------------------------------------------------
            Variables
        ------------------------------------------------------------- */
        var leftArrow = '<i class="fa fa-chevron-left"></i>';
        var rightArrow = '<i class="fa fa-chevron-right"></i>';

        /* -------------------------------------------------------------
            search-menu-bar
        ------------------------------------------------------------- */
        if ($('.search-menu-btn').length) {
            $('.search-menu-btn').on('click', function() {
                $('.search-bar').slideToggle("slow");
                $(this).toggleClass("actives");
                $('.search-menu-btn .search-buttom > i').toggleClass("fa-seach fa-times");
            });
        }

        /* -------------------------------------------------------------
            Scroll To Top
        ------------------------------------------------------------- */
        $.scrollUp({
            scrollText: '<i class="fa fa-arrow-up"></i>',
        });



    });
})(jQuery);